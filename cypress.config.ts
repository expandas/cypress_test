const { defineConfig } = require("cypress");

module.exports = defineConfig({
  viewportHeight: 1080,
  viewportWidth: 1920,
  defaultCommandTimeout: 2000,

  e2e: {
    baseUrl: 'https://www.viversum.de',
    excludeSpecPattern: '**/examples/*',
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
});
