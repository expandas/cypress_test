import {registrationLocators, routeLocators} from "../support/locators";
import {mockUser} from "../support/mock";

describe('Test registration', () => {
  beforeEach(() => {
    cy.visit(routeLocators.registration);
    cy.getElem(registrationLocators.form).as('form');
    cy.intercept('POST', '**/register').as('registrationRequest');
  })


  it('filled from submitted ', () => {
    cy.get('@form').getElem(registrationLocators.username).type(mockUser.username);
    cy.get('@form').getElem(registrationLocators.password).type(mockUser.password);
    cy.get('@form').getElem(registrationLocators.submitButton).click();

    cy.wait('@registrationRequest')
        .then((res) => {
          // check cookie/session or another credentials
          cy.url().should('contain', routeLocators.home);
    });
  })


  it('empty from not submitted ', () => {
    cy.get('@form').getElem(registrationLocators.submitButton).click();

    cy.get('@form').should('exist',registrationLocators.usernameError);
    cy.get('@form').should('exist',registrationLocators.passwordError);

    cy.wait(5000)
  })


  it ('from without email not submitted', () => {
    cy.get('@form').getElem(registrationLocators.password).type(mockUser.password);
    cy.get('@form').getElem(registrationLocators.submitButton).click();

    cy.get('@form').should('exist',registrationLocators.usernameError);

    cy.wait(5000)
  })


  it ('from without password not submitted', () => {
    cy.getElem(registrationLocators.username).type(mockUser.username);
    cy.getElem(registrationLocators.submitButton).click();

    cy.getElem(registrationLocators.form).should('exist',registrationLocators.passwordError);

    cy.wait(5000)
  })
})