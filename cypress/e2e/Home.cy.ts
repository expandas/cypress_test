import {homeLocators, routeLocators} from "../support/locators";

describe('Test Homepage', () => {
  beforeEach(() => {
    cy.visit('/')
  })


  it('Registration button moves to registration form page', () => {
    cy.getElem(homeLocators.registrationButton).click();
    cy.wait(3000);
    cy.url().should('include', routeLocators.registration);
  })


  it('Login button moves to login form page', () => {
    cy.getElem(homeLocators.loginButton).click();
    cy.wait(3000);
    cy.url().should('include', routeLocators.login);
  })


  it ('Move search page witch selected filters', () => {
    cy.getElem(homeLocators.topicFilterSelect).as('topicFilterSelect');
    cy.get('@topicFilterSelect').click();
    cy.get('@topicFilterSelect').getElem(homeLocators.topicFilter.LIEBE).click();

    cy.getElem(homeLocators.productTypeFilter.CALL).click();

    cy.getElem(homeLocators.priceFilter).as('priceFilter');
    cy.get('@priceFilter').click();
    cy.get('@priceFilter').get(homeLocators.priceFilterInput)
        .invoke('val').as('priceFilterInputValue');

    cy.getElem(homeLocators.searchButton).click();

    cy.location({timeout: 5000})
        .should(($url) => {
          expect($url.search)
              .to.eq('?topic=451&productType=CALL&maxPrice=5.37&freeOnly=false')
          })
    })
})