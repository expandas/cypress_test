export const homeLocators = {
  loginButton: 'navbar-login',
  registrationButton: 'navbar-registration',

  topicFilterSelect: 'topic-filter',
  topicFilter: {
    ALL: 'topic-filter-ALL-option',
    LIEBE: 'topic-filter-451-option',
    EMOTION: 'topic-filter-457-option',
    BERUF: 'topic-filter-463-option',
    FAMILIE: 'topic-filter-469-option',
  },
  productTypeFilter: {
    ALL: 'productType-filter-ALL-option',
    CALL: 'productType-filter-CALL-option',
    CHAT: 'productType-filter-CHAT-option',
    ANONYMOUS: 'productType-filter-ANONYMOUS-option',
  },
  priceFilter: 'price-filter-base',
  priceFilterSlider: 'div[role="slider"]',
  priceFilterInput: 'input',
  searchButton: 'filters-redirect-link',
}

export const registrationLocators = {
  form: 'registration-form',
  username: 'username',
  password: 'password',
  submitButton: 'register-form-submit-button',

  usernameError: 'username-error',
  passwordError: 'password-error',
}

export const routeLocators = {
  home: '/',
  registration: '/registrierung',
  login: '/login'
}