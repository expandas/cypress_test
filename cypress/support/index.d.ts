/// <reference types="cypress" />

declare namespace Cypress {
  interface Chainable<Subject> {
    getElem(value: string): Chainable<Subject>;
    formatTopicFilter(value: string): string;
  }
}