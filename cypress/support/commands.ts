Cypress.Commands.add('getElem', (value) => {
  return cy.get(`[data-testid=${value}]`)
});

Cypress.Commands.add('formatTopicFilter', (topicFilter) => {
  return topicFilter.split('-').filter((elem) => elem !== 'filter' && elem !== 'option').join('-');
});