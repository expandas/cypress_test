# cypress_test
## Test cases
- go to Login page by click Login on home
- tests for Login form
- move from Home to Search page with selected filters

## Getting started

Test on `https://www.viversum.de/`

use `npm run test:open` to open Cypress;
